package me.jarno.orthodontiepraktijkenbackend;

import me.jarno.orthodontiepraktijkenbackend.models.DentalBrace;
import me.jarno.orthodontiepraktijkenbackend.models.OrthodontiePraktijk;
import me.jarno.orthodontiepraktijkenbackend.models.Patient;
import me.jarno.orthodontiepraktijkenbackend.repositories.DentalBraceRepository;
import me.jarno.orthodontiepraktijkenbackend.repositories.PatientRepository;
import me.jarno.orthodontiepraktijkenbackend.repositories.PraktijkRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    private PraktijkRepository praktijkRepository;
    private PatientRepository patientRepository;
    private DentalBraceRepository dentalBraceRepository;

    public DataLoader(PraktijkRepository praktijkRepository, PatientRepository patientRepository, DentalBraceRepository dentalBraceRepository) {
        this.praktijkRepository = praktijkRepository;
        this.patientRepository = patientRepository;
        this.dentalBraceRepository = dentalBraceRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.praktijkRepository.save(new OrthodontiePraktijk("Top Orthodontie","Bisschopstraat 37-39", "Enschede", "7513AJ", "0534318185", "info@beugel.nu","beugel.nu"));
        this.praktijkRepository.save(new OrthodontiePraktijk("Blablabla praktijk","Bisschopstraat 37-39", "Enschede", "7513AJ", "0534318185", "beugel@gmail.com","beugel.nu"));
        this.praktijkRepository.save(new OrthodontiePraktijk("Euregio","Bisschopstraat 37-39", "Enschede", "7513AJ", "0534318185", "gsdag@gnasdi.com","beugel.nu"));

        this.patientRepository.save(new Patient("Jarno Manders", "2002-01-28", "Sladenhuishoek 15", "Enschede", "7546BP", "0610289530", "jarno.manders@gmail.com"));
        this.patientRepository.save(new Patient("Pippi Langkous", "1938-03-10", "Villa Kakelbont 1", "Kneippbyn", "1234AB", "0612345678", "pippilangkous@gmail.com"));

        this.dentalBraceRepository.save(new DentalBrace("Aligner", 1500.00, "resources/Aligner.jpeg"));
        this.dentalBraceRepository.save(new DentalBrace("Slotjes", 2500.00, "resources/Slotjes.png"));
    }
}
