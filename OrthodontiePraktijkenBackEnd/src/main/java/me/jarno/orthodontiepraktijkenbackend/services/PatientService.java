package me.jarno.orthodontiepraktijkenbackend.services;

import me.jarno.orthodontiepraktijkenbackend.models.Patient;
import me.jarno.orthodontiepraktijkenbackend.repositories.PatientRepository;

import java.util.List;

public class PatientService {
    private final PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> getPatients() {
        return patientRepository.findAll();
    }

    public Patient getPatient(Long id) {
        return patientRepository.findById(id).orElse(null);
    }
}
