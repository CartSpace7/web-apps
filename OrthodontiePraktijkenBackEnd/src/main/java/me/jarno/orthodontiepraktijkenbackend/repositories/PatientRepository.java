package me.jarno.orthodontiepraktijkenbackend.repositories;

import me.jarno.orthodontiepraktijkenbackend.models.OrthodontiePraktijk;
import me.jarno.orthodontiepraktijkenbackend.models.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {
    @Override
    List<Patient> findAll();

}
