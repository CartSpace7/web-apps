package me.jarno.orthodontiepraktijkenbackend.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class OrthodontiePraktijk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String name;
    @Column
    private String address;
    @Column
    private String city;
    @Column
    private String postalCode;
    @Column
    private String phoneNumber;
    @Column
    private String email;
    @Column
    private String website;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Patient> patients;

    public OrthodontiePraktijk(String name, String address, String city, String postalCode, String phoneNumber,
                               String email, String website) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.website = website;
    }

    public OrthodontiePraktijk() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

}
