package me.jarno.orthodontiepraktijkenbackend.controllers;

import me.jarno.orthodontiepraktijkenbackend.services.PraktijkService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.config.Task;
import org.springframework.web.bind.annotation.*;
import me.jarno.orthodontiepraktijkenbackend.models.OrthodontiePraktijk;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/praktijken")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PraktijkController {
    private final PraktijkService praktijkService;

    public PraktijkController(PraktijkService praktijkService) {
        this.praktijkService = praktijkService;
    }

    @GetMapping()
    public List<OrthodontiePraktijk> getPraktijken() {
       return praktijkService.getPraktijken();
    }

    @GetMapping("/{id}")
    public OrthodontiePraktijk getPraktijk(@PathVariable Long id) {
        return praktijkService.getPraktijk(id);
    }

    @PostMapping("/add-praktijk")
    public ResponseEntity<Object> postPraktijk(OrthodontiePraktijk orthodontiePraktijkData, HttpSession session) {
        OrthodontiePraktijk orthodontiePraktijk = new OrthodontiePraktijk(orthodontiePraktijkData.getName(),
                orthodontiePraktijkData.getAddress(), orthodontiePraktijkData.getCity(),
                orthodontiePraktijkData.getPostalCode(), orthodontiePraktijkData.getPhoneNumber(),
                orthodontiePraktijkData.getEmail(), orthodontiePraktijkData.getWebsite());

        praktijkService.addPraktijk(orthodontiePraktijk);
        //praktijkService.addPraktijk(orthodontiePraktijk);

        HttpHeaders taskHeaders = new HttpHeaders();
//        taskHeaders.add("Location", "/task");


        session.setAttribute("update", true);
        return new ResponseEntity<>(taskHeaders, HttpStatus.CREATED);
    }

}
