package me.jarno.orthodontiepraktijkenbackend.services;

import me.jarno.orthodontiepraktijkenbackend.models.OrthodontiePraktijk;
import me.jarno.orthodontiepraktijkenbackend.repositories.PraktijkRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PraktijkService {
    private final PraktijkRepository praktijkRepository;

    public PraktijkService(PraktijkRepository praktijkRepository) {
        this.praktijkRepository = praktijkRepository;
    }

    public List<OrthodontiePraktijk> getPraktijken() {
        return praktijkRepository.findAll();
    }

    public OrthodontiePraktijk getPraktijk(Long id) {
        return praktijkRepository.findById(id).orElse(null);
    }

    public void addPraktijk(OrthodontiePraktijk orthodontiePraktijk) {
        praktijkRepository.save(orthodontiePraktijk);
    }
}
