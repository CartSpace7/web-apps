package me.jarno.orthodontiepraktijkenbackend.services;

import me.jarno.orthodontiepraktijkenbackend.models.DentalBrace;
import me.jarno.orthodontiepraktijkenbackend.repositories.DentalBraceRepository;

import java.util.List;

public class DentalBraceService {
    private final DentalBraceRepository dentalBraceRepository;

    public DentalBraceService(DentalBraceRepository dentalBraceRepository) {
        this.dentalBraceRepository = dentalBraceRepository;
    }

    public List<DentalBrace> getDentalBraces() {
        return dentalBraceRepository.findAll();
    }

    public DentalBrace getDentalBrace(Long id) {
        return dentalBraceRepository.findById(id).orElse(null);
    }
}
