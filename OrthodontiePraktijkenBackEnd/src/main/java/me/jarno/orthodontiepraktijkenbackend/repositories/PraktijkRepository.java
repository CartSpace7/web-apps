package me.jarno.orthodontiepraktijkenbackend.repositories;

import me.jarno.orthodontiepraktijkenbackend.models.OrthodontiePraktijk;
import org.aspectj.weaver.ast.Or;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PraktijkRepository extends CrudRepository<OrthodontiePraktijk, Long> {
    //Find all praktijken
    @Override
    List<OrthodontiePraktijk> findAll();

}
