package me.jarno.orthodontiepraktijkenbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrthodontiePraktijkenBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrthodontiePraktijkenBackEndApplication.class, args);
    }

}
